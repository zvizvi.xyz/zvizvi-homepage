import angular from 'angular';
import appsComponent from './apps.component';

let appsModule = angular.module('apps', [
])
  .component('apps', appsComponent)
  .name;

export default appsModule;
